package process;

import objetgraph.Panneau;

// Thread qu'i s'occupe de l'animation des balles.

public class AfficheThread extends Thread{
	
	Panneau p; 
	boolean action = true;
	
	public AfficheThread(Panneau p){
		this.p = p;
	}
	public void run() {
		while(true) {
			try {p.animation();} 
			catch (InterruptedException e) {e.printStackTrace();}
		}
	}
}
