package process;

import objetgraph.Number;

// Thread qui s'occupe de l'affichage du temps.

public class Clock extends Thread {
	
	Number t;
	
	public Clock(Number t) {
		super();
		this.t = t;
	}
	
	public void setRun(boolean b) {
		t.setRun(b);
	}
	public void run() {
		while(true) {
			try {
				t.clock();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
