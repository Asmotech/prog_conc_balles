package process;
import objetgraph.Fenetre;


// instanciation des threads principaux et de la fenêtre principale

public class Main {

	public static void main(String[] args) {
		Fenetre fen = new Fenetre();
		
		AfficheThread at = new AfficheThread(fen.pan); // gestion de l'animation
		Clock cl = new Clock(fen.pan.timer); // gestion du Timer
		Score sc = new Score(fen.pan); // gestion du score
		
		sc.setDaemon(true);
		sc.start();
		
		cl.setDaemon(true);
		cl.start();
		
		at.setDaemon(true);
		at.start();
	}
}
