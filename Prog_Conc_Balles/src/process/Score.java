package process;

import objetgraph.Panneau;

// Thread qui gère l'affichge du score.

public class Score extends Thread{
	Panneau p;
	
	public Score(Panneau p2) {
		super();
		this.p = p2;
	}
	
	public void run() {
		while(true) {
			try {p.afficheScore();} 
			catch (InterruptedException e) {e.printStackTrace();}
		}
	}
}
