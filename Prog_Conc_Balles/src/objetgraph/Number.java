package objetgraph;

import javax.swing.JLabel;

// Class qui hérite de JLabel, sers à représenté le temps.

@SuppressWarnings("serial")
public class Number extends JLabel{
	Integer nb;
	boolean run;
	
	public Number() {
		super();
		run = true;
		nb = 0;
		this.setText("Temps : "+nb.toString());
	}
	
	public void setRun(boolean b) { run = b;}
	
	public synchronized void clock() throws InterruptedException {
		if(!run)wait();
		Thread.sleep(1000);
		nb++;
		this.setText("Temps : "+nb.toString());
	}
}
