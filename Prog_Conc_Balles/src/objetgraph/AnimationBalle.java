package objetgraph;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

@SuppressWarnings("serial")
public class AnimationBalle extends JPanel{
	
	List<Balles> balles = new ArrayList<Balles>();  // choix de stocké les balles dans une liste.
	
	public AnimationBalle() {
		super();
		balles.add(new Balles(Color.red, 100, 200)); // balle de départ.
	}
	
	/**
	 * 	Panneau gérant l'affichage des balles de la liste.
	 */
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		if(balles.size() != 0) {
			for (int i = 0; i< balles.size(); i++) {
				g.setColor(balles.get(i).getColor());
				g.fillOval(balles.get(i).getPosX(),balles.get(i).getPosY(), 30, 30);
			}
		}
	}
}
