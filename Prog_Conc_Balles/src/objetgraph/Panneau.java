package objetgraph;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

//Panneau servant de conteneur principal afin de différencier le thread de l'animation de celui de la fenetre principale  

@SuppressWarnings("serial")
public class Panneau extends JPanel implements ActionListener{
	
	// creation des différents conteneurs de l'interface graphique
	// top pour le conteneur du temps et du scores qui se trouvent en haut
	// jeu pour le conteneur de l'affichage des balles
	// bottom pour le conteneur des boutons se situant en bas
	
	AnimationBalle jeu = new AnimationBalle();
	
	JPanel bottom = new JPanel();
	JPanel top = new JPanel();
	
	// pause pour mettre le jeu en pause 
	// plus pour ajouter une balle 
	// minus pour supprimer une balle
	
	JButton pause = new JButton("Pause");
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	
	// timer pour l'affichage du temps
	// Score pour l'affichage du score
	
	public Number timer = new Number();
	public JLabel tabScore = new JLabel("Score : 0");
	
	int score;
	boolean affiche;
	
	
	public Panneau() {
		super();
		score = 0;
		affiche = true;
		
		// gestion de l'affichage des boutton situés en bas
		
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.LINE_AXIS));
		bottom.add(Box.createHorizontalGlue());
		bottom.add(pause);
		bottom.add(Box.createRigidArea(new Dimension(10,0)));
		bottom.add(plus);
		bottom.add(Box.createRigidArea(new Dimension(10,0)));
		bottom.add(minus);
		bottom.add(Box.createHorizontalGlue());
		
		// gestion de l'affichage des textes situés en haut
		
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		top.add(Box.createHorizontalGlue());
		top.add(tabScore);
		top.add(Box.createRigidArea(new Dimension(30,0)));
		top.add(timer);
		top.add(Box.createHorizontalGlue());
		
		pause.addActionListener(this);
		plus.addActionListener(this);
		minus.addActionListener(this);
		
		this.setLayout(new BorderLayout());
		
		this.add(jeu, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.PAGE_END);
		this.add(top, BorderLayout.PAGE_START);
		
	}
	
	// Fonction qui gère le déplacement des balles à l'écran
	public synchronized void animation() throws InterruptedException {
		
		List<Object> toSup = new ArrayList<Object>();
		
		while(!affiche)wait();
		// gère le rebond de chaque balle sur les bords de la fenêtre et leur déplacement.
		for (int i = 0; i < jeu.balles.size(); i++) {
			
			if ( jeu.balles.get(i).getPosX() < 1) jeu.balles.get(i).setBackX(false);
			if ( jeu.balles.get(i).getPosY() < 1) jeu.balles.get(i).setBackY(false);
			
			
			if ( jeu.balles.get(i).getPosX() > jeu.getWidth()-30) jeu.balles.get(i).setBackX(true);
			if ( jeu.balles.get(i).getPosY() > jeu.getHeight()-30) jeu.balles.get(i).setBackY(true);
			
			if (!jeu.balles.get(i).getBackX()) jeu.balles.get(i).setPosX( jeu.balles.get(i).getPosX() + 1 );
			else jeu.balles.get(i).setPosX( jeu.balles.get(i).getPosX() - 1 );
			
			if (!jeu.balles.get(i).getBackY()) jeu.balles.get(i).setPosY( jeu.balles.get(i).getPosY() + 1 );
			else jeu.balles.get(i).setPosY( jeu.balles.get(i).getPosY() - 1 );
					
		}
		
		for(int i=0 ; i < jeu.balles.size(); i++) {
			for(int j= jeu.balles.size()-1; j>i ; j--) {
				
				int x1 = jeu.balles.get(i).getPosX(), y1 = jeu.balles.get(i).getPosY();
				int x2 = jeu.balles.get(j).getPosX(), y2 = jeu.balles.get(j).getPosY();
				
				if( ((x2 >= x1 && x2 <= (x1+30)) || ((x2+30) >= x1 && (x2+30) <= (x1+30))) && ((y2 >= y1 && y2 <= (y1+30)) || ((y2+30) >= y1 && (y2+30) <= (y1+30))) ) {
					this.score++;
					
					plus.setEnabled(true); // vu que 2 balles vont etres supprimer on reactive le bouton 
					
					toSup.add(jeu.balles.get(i));
					toSup.add(jeu.balles.get(j));
				}
			}
		}
		
		for(int i= 0; i < toSup.size(); i++) {  // Ce bout de code sert à évité les dépassements d'index de liste 
			jeu.balles.remove(toSup.get(i));	// dut à la supression d'objet de la liste dans le for{}
		}
		toSup.clear();
		
		jeu.repaint();
		this.repaint();
		notifyAll();
		Thread.sleep(5);
		wait(); // en Attente de afficheScore()
	}
	
	public synchronized void afficheScore() throws InterruptedException {
		tabScore.setText("Score : "+ this.score);
		notifyAll();
		wait(); // en Attente de animation()
	}
	
	// Gestion des évènements des boutons plus, minus et pause 
	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		if(e.getSource() == pause) {
			if(pause.getText().equals("Pause")) {
				pause.setText("Play ");	// change le texte du bouton sur Play
				affiche = false;	// stop l'animation
				timer.setRun(false);	// stop le timer
			}
			else {
				pause.setText("Pause"); // change le texte du bouton sur Pause					
				affiche = true;	// Permet à l'animation de savoir quel poura se lancer
				timer.setRun(true);
				synchronized(this) {notifyAll();}
			}
		}
		
		if(e.getSource() == plus) { // détection du bouton appuyé
			
			int posx = (int) Math.round(Math.random()*jeu.getWidth()+1)-1; // faire apparaitre des balles de manière aléatoire à l'écran
			int posy = (int) Math.round(Math.random()*jeu.getHeight()+1)-1;
			
			int rouge, bleu, vert;
			rouge = (int) (Math.random()*256);
			bleu = (int) (Math.random()*256);
			vert = (int) (Math.random()*256); // tirage d'une couleur aléatoire
			
			jeu.balles.add(new Balles(new Color(rouge, vert, bleu), posx, posy)); // on ajoute la balle créer à la liste des balles
			
			if(jeu.balles.size()==20)plus.setEnabled(false); // limite de balles à l'écran fixé à 20.
			
			minus.setEnabled(true); // on dégrise le bouton minus
		}
		
		if(e.getSource() == minus) {
			int ind = (int) (Math.random()*jeu.balles.size());
			jeu.balles.remove(ind);
			if(jeu.balles.size()==0)minus.setEnabled(false); // si il n'y as plus de balle on grise le bouton minus;
			
			plus.setEnabled(true); // dégrisement de plus
		}
	}
	
}
