package objetgraph;

import java.awt.Color;

// Classe représentante les balles avec leur couleur, leur sens de déplacements et leurs position.

public class Balles {
	
	private Color c;
	private int posX, posY;
	private boolean backX, backY;
	
	public Balles(Color c, int x, int y) {
		this.c = c; this.posX = x; this.posY = y;
		this.backX = false; this.backY = false;
	}
	
	public Balles setBackX(boolean b) {
		this.backX = b;
		return this;
	}
	
	public boolean getBackX() {
		return this.backX;
	}
	
	public Balles setBackY(boolean b) {
		this.backY = b;
		return this;
	}
	
	public boolean getBackY() {
		return this.backY;
	}
	
	public Balles setColor(Color c) {
		this.c = c;
		return this;
	}
	
	public Color getColor() {
		return this.c;
	}
	
	public Balles setPosX(int x) {
		this.posX = x;
		return this;
	}
	
	public int getPosX() {
		return this.posX;
	}
	
	public int getPosY() {
		return this.posY;
	}
	
	public Balles setPosY(int y) {
		this.posY = y;
		return this;
	}
	
}
