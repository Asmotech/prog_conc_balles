package objetgraph;

import javax.swing.*;

@SuppressWarnings("serial")
public class Fenetre extends JFrame{
	
	// fenêtre pour l'affichage finale
	// pan pour le conteneur principal de la fenetre.
	 
	public Panneau pan = new Panneau();

	public Fenetre() {
		
		this.setTitle("Animation");
		this.setSize(400,600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);
		
		this.setContentPane(pan);
		
		this.setVisible(true);
		
	}
}
